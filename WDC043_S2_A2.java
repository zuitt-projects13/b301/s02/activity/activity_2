package com.zuitt;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args) {

        // * Array
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

        // * ArrayList
        System.out.println("-----------------------------------------------------");

        ArrayList<String> strArray =  new ArrayList<>();
        strArray.add("John");
        strArray.add("Jane");
        strArray.add("Chloe");
        strArray.add("Zoey");

        System.out.println("My friends are: " + strArray);


        // * HashMaps
        System.out.println("-----------------------------------------------------");

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of: " + inventory);

    }
}
